FROM continuumio/miniconda3:latest

MAINTAINER Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>


RUN dpkg --add-architecture i386 && apt-get update && apt-get install -qq software-properties-common build-essential wget git && apt install -y wine32 && which wine && conda update --yes conda
RUN useradd -m py
WORKDIR /src


COPY environment.yml /src/
RUN conda env create -q -n remaining-carbon-budget-frameworks-2019 -f environment.yml


COPY . /src/

ENV DATA_DIR /home/py/data
ENV OUTPUT_DIR /home/py/output


# Ensure that the correct conda environment is used
RUN sed -i 's/conda activate base/conda activate remaining-carbon-budget-frameworks-2019/g' ~/.bashrc
RUN touch setup.py

# Create new conda environment and install our requirements too
# and copy data and do any pre-processing required
RUN bash -c '. /opt/conda/etc/profile.d/conda.sh && conda activate remaining-carbon-budget-frameworks-2019 && cp -r /src/data $DATA_DIR && conda install -c conda-forge gcc_linux-64 && make -B data && rm -rf /src/* && conda list && pip list' && conda clean -a -y
