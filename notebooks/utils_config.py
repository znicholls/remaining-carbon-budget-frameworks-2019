import os
from distutils.util import strtobool

TEST_RUN = False or strtobool(os.getenv("CI", "False"))
