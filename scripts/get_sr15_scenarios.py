# Would be ideal if we could just pull SR1.5 database data but ongoing issues there
# https://github.com/iiasa/ipcc_sr15_scenario_analysis/issues/20
from os import listdir
from os.path import dirname, join, splitext
from argparse import ArgumentParser
import datetime as dt


import pandas as pd
from pymagicc.io import MAGICCData
from openscm.scmdataframe import df_append, ScmDataFrame


here = dirname(__file__)


def get_sr15_scenarios(output, input_dir, scenlist, add_meta=False):
    valid_scens_df = pd.read_csv(scenlist)
    valid_combos = [tuple(v) for v in valid_scens_df[["model", "scenario"]].values]
    dfs = []
    if add_meta == "True":
        add_meta = True
    for file in listdir(input_dir):
        try:
            model = file.split("_")[1]
            scenario = "_".join(file.split("_")[2:])
        except ValueError:
            if "C-ROADS" in file:
                model = "C-ROADS"
                scenario = "-".join(file.split("-")[2:])
            elif "En-ROADS-96" in file:
                model = "En-ROADS-96"
                scenario = "_".join(file.split("_")[2:])
            elif "WEM" in file:
                model = "WEM"
                scenario = "_".join(file.split("_")[2:])
            else:
                import pdb

                pdb.set_trace()

        model = model.replace("IPCCSR15_", "")
        scenario = scenario.replace(".SCEN", "").replace("_GAS", "")
        valid_combo = True
        if (model, scenario) not in valid_combos:
            if not add_meta:
                continue
            else:
                valid_combo = False

        model = model.replace(" ", "_").replace(".", "_")
        data = MAGICCData(join(input_dir, file))
        data.set_meta(scenario.replace(".SCEN", ""), name="scenario")
        data.set_meta(model, name="model")
        if add_meta:
            data.set_meta(valid_combo, name="in_final_list")
        dfs.append(data)

    if not add_meta:
        assert len(dfs) == 411
    dfs = df_append(dfs)

    t_2100 = dt.datetime(2100, 1, 1, 0, 0, 0)
    t_2105 = dt.datetime(2105, 1, 1, 0, 0, 0)
    dfs = dfs.timeseries()[~dfs.timeseries().loc[:, t_2100].isnull()]
    dfs.loc[:, t_2105] = dfs.apply(
        lambda x: x[t_2100] if pd.isnull(x[t_2105]) else x[t_2105], axis="columns"
    )
    dfs = ScmDataFrame(dfs).filter(year=range(2000, 2106))

    dfs.to_csv(output)


def main():
    parser = ArgumentParser(
        prog="get-sr15-scenarios", description="Retrieve SR15 scenarios for this study"
    )

    parser.add_argument("output", help="File in which to save the scenarios")
    parser.add_argument(
        "--inputdir",
        help="Directory from which to load the files",
        default=join(here, "..", "data", "sr15_scenfiles", "scenfiles"),
    )
    parser.add_argument(
        "--scenlist",
        help="File which lists valid SCEN files from SR1.5",
        default=join(here, "..", "data", "sr15_scenfiles", "final411scenarios.csv"),
    )
    parser.add_argument(
        "--add-meta",
        help="Add meta column which defines whether data should be marked as in final 411 scenarios or not",
        default=False,
    )
    args = parser.parse_args()

    get_sr15_scenarios(args.output, args.inputdir, args.scenlist, args.add_meta)


if __name__ == "__main__":
    main()
