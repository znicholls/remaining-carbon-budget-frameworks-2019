.DEFAULT_GOAL := help

ifndef CONDA_PREFIX
$(error Conda not active, please install conda and then create an environment with `conda create --name remaining-carbon-budget-frameworks-2019` and activate it with `conda activate remaining-carbon-budget-frameworks-2019`))
else
ifeq ($(CONDA_DEFAULT_ENV),base)
$(error Do not install to conda base environment. Source a different conda environment e.g. `conda activate remaining-carbon-budget-frameworks-2019` or `conda create --name remaining-carbon-budget-frameworks-2019` and rerun make including the `-B` flag))
endif
endif

PYTHON=$(CONDA_PREFIX)/bin/python
CONDA_ENV_FILE=environment.yml
SCONS_FILE=SConstruct


SCRIPTS_DIR=scripts
DATA_DIR=data
NOTEBOOKS_DIR=notebooks
FIGURES_DIR=figures
OUTPUT_DIR=output


FILES_TO_FORMAT_PYTHON=$(SCRIPTS_DIR) $(SCONS_FILE) $(NOTEBOOKS_DIR)/*.py


SR15_OPENSCM_SCENARIOS_GENERATION_SCRIPT=$(SCRIPTS_DIR)/get_sr15_scenarios.py
SR15_SCEN_FILES_DIR=$(DATA_DIR)/sr15_scenfiles/scenfiles
SR15_VALID_SCENARIOS_LIST=$(DATA_DIR)/sr15_scenfiles/final411scenarios.csv
SR15_OPENSCM_SCENARIOS_DIR=$(DATA_DIR)/sr15_scenarios
SR15_OPENSCM_SCENARIOS=$(SR15_OPENSCM_SCENARIOS_DIR)/sr15_scenarios.csv

ARCHIVE_FILE=remaining-carbon-budget-frameworks-2019.tar.gz

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([\$$\(\)a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT


help:  ## print short description of each target
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)
	@echo ""
	@make variables

.PHONY: all
all:  $(SR15_OPENSCM_SCENARIOS)  ## make the full manuscript in pdf format
	export NOTEBOOKS_DIR=$(NOTEBOOKS_DIR); \
	scons --tree=prune,status

.PHONY: archive
archive: $(ARCHIVE_FILE)  ## create an archive of the files to upload to zenodo

$(ARCHIVE_FILE):
	mkdir -p remaining-carbon-budget-frameworks-2019-tar-dir
	cp ./{.dockerignore,.gitattributes,.gitignore,.gitlab-ci.yml,Dockerfile,Makefile,README.md,SConstruct,environment.yml} remaining-carbon-budget-frameworks-2019-tar-dir
	cp -r data remaining-carbon-budget-frameworks-2019-tar-dir/
	cp -r figures remaining-carbon-budget-frameworks-2019-tar-dir/
	cp -r notebooks remaining-carbon-budget-frameworks-2019-tar-dir/
	cp -r outputs remaining-carbon-budget-frameworks-2019-tar-dir/
	cp -r regression remaining-carbon-budget-frameworks-2019-tar-dir/
	cp -r scripts remaining-carbon-budget-frameworks-2019-tar-dir/

	tar -czvf $@ remaining-carbon-budget-frameworks-2019-tar-dir

.PHONY: data
data: $(CONDA_PREFIX) $(SR15_OPENSCM_SCENARIOS)  ## collect and process all the input data required for the manuscript

$(SR15_OPENSCM_SCENARIOS): $(SR15_OPENSCM_SCENARIOS_GENERATION_SCRIPT) $(SR15_SCEN_FILES_DIR) $(SR15_VALID_SCENARIOS_LIST) $(CONDA_PREFIX)
	mkdir -p $(SR15_OPENSCM_SCENARIOS_DIR)
	$(PYTHON) $< --inputdir $(SR15_SCEN_FILES_DIR) --scenlist $(SR15_VALID_SCENARIOS_LIST) $@

.PHONY: conda-environment
conda-environment: $(CONDA_PREFIX)  ## create conda environment for generating the paper
$(CONDA_PREFIX): $(CONDA_ENV_FILE)
	$(CONDA_EXE) env update --name $(CONDA_DEFAULT_ENV) -f $(CONDA_ENV_FILE)
	touch $(CONDA_PREFIX)

.PHONY: black
black: $(CONDA_PREFIX)  ## auto-format the Python scripts
	@status=$$(git status --porcelain $(FILES_TO_FORMAT_PYTHON)); \
	if test "x$${status}" = x; then \
		$(CONDA_PREFIX)/bin/black $(FILES_TO_FORMAT_PYTHON); \
	else \
		echo Not trying any formatting. Working directory is dirty ... >&2; \
	fi

.PHONY: clean
clean:  ## remove the latex compilation byproducts and the conda environment
	make clean-latex
	make clean-conda-environment

.PHONY: clean-latex
clean-latex:  ## clean all the products of building converting latex to pdf
	find . -type f -name '*.aux' -delete
	find . -type f -name '*.log' -delete
	find . -type f -name '*.bbl' -delete
	find . -type f -name '*.blg' -delete
	find . -type f -name '*.fls' -delete
	find . -type f -name '*.bcf' -delete
	find . -type f -name '*.out' -delete
	find . -type f -name '*.xml' -delete

.PHONY: clean-conda-environment
clean-conda-environment:  ## remove the conda environment
	rm -rf $(CONDA_PREFIX)

.PHONY: variables
variables:  ## display the value of all variables in the Makefile
	@echo CONDA_PREFIX: $(CONDA_PREFIX)
	@echo PYTHON: $(PYTHON)
	@echo CONDA_DEFAULT_ENV: $(CONDA_DEFAULT_ENV)
	@echo CONDA_PREFIX: $(CONDA_PREFIX)
	@echo CONDA_ENV_FILE: $(CONDA_ENV_FILE)
	@echo SCONS_FILE: $(SCONS_FILE)
	@echo ""
	@echo DATA_DIR: $(DATA_DIR)
	@echo SCRIPTS_DIR: $(SCRIPTS_DIR)
	@echo NOTEBOOKS_DIR: $(NOTEBOOKS_DIR)
	@echo FIGURES_DIR: $(FIGURES_DIR)
	@echo OUTPUT_DIR: $(OUTPUT_DIR)
	@echo ""
	@echo SR15_OPENSCM_SCENARIOS_GENERATION_SCRIPT: $(SR15_OPENSCM_SCENARIOS_GENERATION_SCRIPT)
	@echo SR15_SCEN_FILES_DIR: $(SR15_SCEN_FILES_DIR)
	@echo SR15_OPENSCM_SCENARIOS_DIR: $(SR15_OPENSCM_SCENARIOS_DIR)
	@echo SR15_OPENSCM_SCENARIOS: $(SR15_OPENSCM_SCENARIOS)
	@echo ""
	@echo FILES_TO_FORMAT_PYTHON: $(FILES_TO_FORMAT_PYTHON)
