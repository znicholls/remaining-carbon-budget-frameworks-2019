import os
import warnings
import datetime

import pandas as pd
from pymagicc import MAGICC6
from pymagicc.io import MAGICCData
from pymagicc.definitions import (
    PART_OF_PRNFILE,
    convert_magicc7_to_openscm_variables,
    convert_magicc6_to_magicc7_variables,
)
from progressbar import progressbar

from openscm.scmdataframe import (
    ScmDataFrame,
    df_append,
)
from openscm.utils import (
    convert_datetime_to_openscm_time,
    convert_openscm_time_to_datetime,
)

SAVE_KEY = "scmdf"
TIME_COLS = ["time"]

def save_df(df, output_path):
    def remove_invalid_chars(instr):
        invalid_chars = ["(", ")", " ", ".", "-", "|"]
        new_name = instr
        for c in invalid_chars:
            new_name = new_name.replace(c, "_")

        return new_name

    sdf = df.copy()
    for col in sdf:
        if col in TIME_COLS:
            sdf[col] = sdf[col].apply(convert_datetime_to_openscm_time)
        if isinstance(col, str):
            sdf = sdf.rename({col: remove_invalid_chars(col)}, axis="columns")

    if isinstance(sdf.index, pd.core.indexes.multi.MultiIndex):
        sdf.index = sdf.index.rename([
            remove_invalid_chars(n) for n in sdf.index.names
        ])

    sdf.to_hdf(
        output_path,
        SAVE_KEY,
        format="table",
        mode="w",
        complevel=9,
        complib="bzip2"
    )


def load_df(filepath):
    loaded = pd.read_hdf(filepath, SAVE_KEY)
    for col in loaded:
        if col in TIME_COLS:
            loaded[col] = loaded[col].apply(convert_openscm_time_to_datetime)

    return loaded


def save_scmdataframe(scmdf, output_path):
    """Save an ScmDataFrame to hdf5

    Parameters
    ----------
    scmdf : :`obj`:ScmDataFrame
        ScmDataFrame instance to save.

    output_path : str
        Path in which to save the file. The key
        is always "scmdf".
    """
    sav = scmdf.timeseries()
    sav.columns = sav.columns.map(
        convert_datetime_to_openscm_time
    )  # required to skip overflow error in mktime...
    save_df(sav, output_path)


def load_scmdataframe(filepath):
    """Load an ScmDataFrame from an hdf5 file saved with save_scmdataframe


    Parameters
    ----------
    filepath : str
        Path from which to load the ScmDataFrame

    Returns
    -------
    :`obj`:ScmDataFrame
        ScmDataFrame instance
    """
    loaded = load_df(filepath)
    loaded.columns = loaded.columns.map(convert_openscm_time_to_datetime)
    return ScmDataFrame(loaded)


def run_magicc6_with_scenarios(scenarios, settings, vars_to_keep=None, test_run=False, from_zero=False):
    # be super careful with temp feedback and conc switches
    if "co2_switchfromconc2emis_year" not in settings:
        warnings.warn("No co2_switchfromconc2emis_year, is this on purpose?")
    if "co2_tempfeedback_switch" in settings and settings["co2_tempfeedback_switch"] != 1:
        warnings.warn("CO2 temp feedback switch is off, is this on purpose?")
    if "co2_tempfeedback_yrstart" not in settings:
        warnings.warn("No co2_tempfeedback_yrstart, is this on purpose?")
    if "co2_fertilization_yrstart" not in settings:
        warnings.warn("No co2_fertilization_yrstart, is this on purpose?")

    if vars_to_keep is None:
        vars_to_keep = [
            "Surface Temperature",
            "Emissions|CO2|MAGICC Fossil and Industrial",
            "Emissions|CO2|MAGICC AFOLU",
            "Inverse Emissions|CO2|MAGICC Fossil and Industrial",
            "Radiative Forcing",
            "Land to Air Flux|CO2|MAGICC Permafrost",
        ]

    if any(["Inverse Emissions" in v for v in vars_to_keep]):
        vars_to_keep.append("INVERSEEMIS")

    res = []
    scen_model_combos = (
        scenarios.timeseries().reset_index()[["scenario", "model"]].drop_duplicates()
    )
    if test_run:
        # make CI and test runs faster
        scen_model_combos = scen_model_combos[:25]
    iterator = scen_model_combos.iterrows()

    with MAGICC6() as magicc:
        for _, (scenario, model) in progressbar(iterator):
            if from_zero:
                magicc.set_zero_config()
            rdf = MAGICCData(
                scenarios.filter(scenario=scenario, model=model).timeseries()
            )

            if "Radiative Forcing|Extra" in rdf["variable"].tolist():
                custom_rf_filename = "CUSTOM_EXTRA_RF.IN"
                magicc.update_config(file_extra_rf=custom_rf_filename)

                custom_rf = rdf.filter(
                    variable="Radiative Forcing|Extra"
                ).timeseries().reset_index()
                custom_rf["todo"] = "SET"
                rf_writer = MAGICCData(custom_rf)
                rf_writer.metadata = {"header": "Custom RF file"}
                rf_writer.write(
                    os.path.join(magicc.run_dir, custom_rf_filename),
                    magicc_version=magicc.version
                )

                rdf = rdf.filter(variable="Radiative Forcing|Extra", keep=False)

                assert settings["rf_total_runmodus"] in ["ALL", "QEXTRA"]
                assert settings["rf_extra_read"] == 1

            single_concs_vars = [
                v for v in rdf["variable"].unique().tolist()
                if v in [
                    "Atmospheric Concentrations|CO2",
                    "Atmospheric Concentrations|CH4",
                    "Atmospheric Concentrations|N2O",
                ]
            ]
            for scv in single_concs_vars:
                cv_name = scv.split("|")[1]
                custom_conc_filename = "CUSTOM_{}_CONC.IN".format(cv_name)
                magicc_args = {
                    "file_{}_conc".format(cv_name.lower()): custom_conc_filename
                }
                magicc.update_config(**magicc_args)

                custom_conc = rdf.filter(variable=scv).timeseries()
                custom_conc_final_year = custom_conc.columns.max().year
                custom_conc = custom_conc.reset_index()
                custom_conc["todo"] = "SET"
                custom_conc_writer = MAGICCData(custom_conc)
                custom_conc_writer.metadata = {
                    "header": "Custom {} concentrations".format(cv_name)
                }
                custom_conc_writer.write(
                    os.path.join(magicc.run_dir, custom_conc_filename),
                    magicc_version=magicc.version
                )

                run_options = ["ALL", "GHG", "ANTHROPOGENIC"]
                if cv_name == "CO2":
                    run_options.append("CO2")
                assert settings["rf_total_runmodus"] in run_options
                assert settings[
                    "{}_switchfromconc2emis_year".format(cv_name.lower())
                ] >= custom_conc_final_year

                rdf = rdf.filter(variable=scv, keep=False)

            openscm_prn_vars = convert_magicc7_to_openscm_variables([
                "{}_CONC".format(v)
                for v in PART_OF_PRNFILE
            ])
            prn_vars = [
                v for v in rdf["variable"].unique().tolist()
                if v in openscm_prn_vars
            ]
            if prn_vars:
                missing_prn_vars_msg = (
                    "if one prn var is supplied, they all must be, full list of "
                    "prn vars is {}".format(openscm_prn_vars)
                )
                all_prn_vars_present = all([v in openscm_prn_vars for v in prn_vars])
                assert all_prn_vars_present, missing_prn_vars_msg

                custom_prn_filename = "CUSTOM_MixingRatios.prn"
                magicc.update_config(file_mhalo_conc=custom_prn_filename)

                # no idea why pre 1850 does weird things...
                warnings.warn("Dropping prn data pre 1850")
                custom_prn_conc = rdf.filter(
                    variable=prn_vars,
                    year=range(1850, 30000)
                ).timeseries()
                custom_prn_conc_final_year = custom_prn_conc.columns.max().year
                custom_prn_conc = custom_prn_conc.reset_index()
                custom_prn_conc["todo"] = "SET"
                custom_prn_conc_writer = MAGICCData(custom_prn_conc)
                custom_prn_conc_writer.metadata = {
                    "header": "Custom Montreal Halogen concentrations"
                }
                custom_prn_conc_writer.write(
                    os.path.join(magicc.run_dir, custom_prn_filename),
                    magicc_version=magicc.version
                )

                run_options = ["ALL", "GHG", "ANTHROPOGENIC"]

                assert settings["rf_total_runmodus"] in run_options
                assert (
                    settings["mhalo_switch_conc2emis_yr"]
                    >= custom_prn_conc_final_year
                )

                rdf = rdf.filter(variable=prn_vars, keep=False)

            fgas_default_filelist = magicc.update_config(
                filename="MAGCFG_USER.CFG"
            )["nml_allcfgs"]["file_fgas_conc"]
            openscm_fgas_vars = convert_magicc7_to_openscm_variables(
                convert_magicc6_to_magicc7_variables([
                    "{}_CONC".format(v.split("_")[1])
                    for v in fgas_default_filelist
                ])
            )
            fgas_vars = [
                v for v in rdf["variable"].unique().tolist()
                if v in openscm_fgas_vars
            ]
            if fgas_vars:
                for fgv in fgas_vars:
                    fgv_name = fgv.split("|")[1]
                    custom_fgv_conc_filename = "CUSTOM_{}_CONC.IN".format(fgv_name)

                    try:
                        fgas_conc_files = magicc.update_config(
                        )["nml_allcfgs"]["file_fgas_conc"]
                    except KeyError:
                        fgas_conc_files = magicc.update_config(
                            filename="MAGCFG_USER.CFG"
                        )["nml_allcfgs"]["file_fgas_conc"]
                    fgas_conc_files[openscm_fgas_vars.index(fgv)] = custom_fgv_conc_filename
                    magicc.update_config(file_fgas_conc=fgas_conc_files)

                    custom_fgv_conc = rdf.filter(variable=fgv).timeseries()
                    custom_fgv_conc_final_year = custom_fgv_conc.columns.max().year
                    custom_fgv_conc = custom_fgv_conc.reset_index()
                    custom_fgv_conc["todo"] = "SET"
                    custom_fgv_conc_writer = MAGICCData(custom_fgv_conc)
                    custom_fgv_conc_writer.metadata = {
                        "header": "Custom {} concentrations".format(fgv_name)
                    }
                    custom_fgv_conc_writer.write(
                        os.path.join(magicc.run_dir, custom_fgv_conc_filename),
                        magicc_version=magicc.version
                    )

                    run_options = ["ALL", "GHG", "ANTHROPOGENIC"]

                    assert settings["rf_total_runmodus"] in run_options
                    assert settings[
                        "fgas_switchfromconc2emis_year"
                    ] >= custom_fgv_conc_final_year

                    rdf = rdf.filter(variable=fgv, keep=False)

            res.append(magicc.run(rdf, only=vars_to_keep, **settings))

    res = df_append(res)
    # clunky, will be upgraded via https://github.com/openclimatedata/pymagicc/issues/248
    for k, v in settings.items():
        res.set_meta(v, k)

    return res
