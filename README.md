# Remaining carbon budget frameworks 2019

Figures: [![figure pipeline status](https://gitlab.com/znicholls/remaining-carbon-budget-frameworks-2019/badges/master/pipeline.svg)](https://gitlab.com/znicholls/remaining-carbon-budget-frameworks-2019/commits/master)

## Getting Started

### Requirements

These are the resources we used to crunch the paper, you'll likely need something similar for it to work for you.

- Ubuntu 18.04 server with ~40GB of RAM (yes, we should have reduced memory requirements but those optimisations didn't quite happen this time, merge requests welcome :D)
- `conda` (`wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh; bash ~/miniconda.sh`)
- `pdflatex` (install with `sudo apt-get install texlive-full`, you might get away with a smaller tex install if space is tight)
- `make` (install with `sudo apt-get install build-essential`)
- `git lfs` (install with `sudo apt-get install git-lfs`)
- `wine` (if on non-Windows server/machine, `sudo dpkg --add-architecture i386;wget -qO - https://dl.winehq.org/wine-builds/winehq.key | sudo apt-key add -;sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main';sudo apt-get update;sudo apt-get install --install-recommends winehq-stable;wine --version`, see [pymagicc](https://github.com/openclimatedata/pymagicc) for instructions on how to install wine on other systems)

We would also recommend install `screen` (`sudo apt install screen`) as crunching takes about 15 hours.
Having said that, a test run can be performed in about 20 minutes (see instructions below).

## Generating the Results

With the requirements above (on some systems you may also need to install `gcc` (Mac OSX) or `gcc_linux-64` (unix) via conda), you need to do two steps:

- `cd remaining-carbon-budget-frameworks-2019; git lfs install; git pull`
- `make -B all` (or `make -B all 2>&1 | tee crunching.log` if you want logs on screen and to a file)
    - if you want to do a test run, do `rm outputs/*;export CI=True;make -B all`

The outputs from `make` should guide you through the required steps to create and activate the correct conda environment.

## Thank you

- [wtbarnes](https://github.com/wtbarnes) for their efforts with https://github.com/wtbarnes/astro_paper_template, this provided the basis for the automation of this repository
- [jhamrick](https://github.com/jhamrick) for their efforts with [nbflow](https://github.com/jhamrick/nbflow)
