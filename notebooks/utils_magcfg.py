from pymagicc.core import MAGICC6
from pymagicc.io import read_cfg_file
from f90nml import Namelist


def get_carbon_cycle_cfgs(cc_files):
    cc = {
        name: read_cfg_file(file)["nml_allcfgs"]
        for name, file in cc_files.items()
    }
    # drop out any core parameters which are used to emulate the carbon cycle model
    # climate but which we don't want to use here
    cc = {
        name: {
            k: v for k, v in cc_config.items()
            if "core" not in k
        }
        for name, cc_config in cc.items()
    }

    with MAGICC6() as magicc:
        default_cfg = magicc.update_config(filename="MAGCFG_USER.CFG")["nml_allcfgs"]

    cc["default"] = Namelist({
        k: default_cfg[k]
        for k in list(cc.values())[0].keys()
    })

    return cc
